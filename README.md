# A hand-holding guide to writing FUSE-based filesystems in Python

![Python FUSE](./img/python-fuse.400.png){width=20%}

## What is this?

[FUSE](https://github.com/libfuse/libfuse) (an interface for userpsace
programs to export a virtual filesystem to the Linux kernel) is a great
tool, and can be used to present many different situations. There are
several good examples that are shipped with the bindings for different
languages (i.e. [almost 20 example programs are available in C for the
reference
implementation](https://github.com/libfuse/libfuse/tree/master/example)).

Python is a very popular language for teaching, and of course, there are
[Python bindings available for
FUSE](https://github.com/libfuse/python-fuse) [PyPI
page](https://pypi.org/project/fuse-python/). Of course, it also has a
[number of available
examples](https://github.com/libfuse/python-fuse/tree/master/example).

With this little project, I want to present a set of examples presenting
FUSE's functionality in a *stepwise fashion*, that is, as a logically
ordered set of steps, guiding you from a micro-filesystem that is
successful to load and does nothing, to one that gives full interaction
and a non-obvious view of a complex set of information. Of course, it is
quite possible we will have some forks along the road, as complexity can
grow in many different directions 😉

Ideally, you will be able to `diff` any of my examples with the
preceding and following ones, and understand what it took me to
implement its functionality (or the next one's).

### Implemented steps of a FUSE filesystem

So far, I have implemented:

1. [1._empty_fs.py](./1._emptyfs.py): Empty filesystem
2. [2._readonly.py](./2._readonly.py): Read-only filesystem in RAM
3. [3._readwrite.py](./3._readwrite.py): Read-write filesystem in
   RAM. File contents are modifiable, but the directory entries are
   immutable (no files can be created or removed)
4. [4._basic_stat_info.py](./4._basic_stat_info.py): Read-write
   filesystem in RAM with meaningful basic `stat()` management. Same as
   the above, but file UID/GID match the user that mounted the
   filesystem and timestamps are meaningful.
5. [5._read_only_passthrough.py](./5._read_only_passthrough.py): Read-only
   filesystem (based on `2._readonly.py`) implementing a _passthrough
   filesystem_, that is, presenting an already mounted portion of your
   filesystem. All objects present in the original filesystem will be presented
   as _read-only_. Symlinks are presented as regular files. You can specify the
   directory to be passed through using `-o src_dir=/a/directory`; if
   unspecified, the current directory will be used.

### Useful bits of code presented as FUSE filesystems

I also intend to present some cool(?) ideas that present a useful
implementation of something non-obvious, stating which of the above
"implemented steps" they derive from.

1. [DNS filesystem](./useful/dnsfs.py): Query DNS by reading from files.

   This module is based on [2._readonly.py](./2_readonly.py): We only
   need to read from (nonexisting!) files to perform our work.

		$ ls -l mnt
		total 1
		dr-xr-xr-x 2 root root   0 Dec 31  1969 A
		dr-xr-xr-x 2 root root   0 Dec 31  1969 AAAA
		dr-xr-xr-x 2 root root   0 Dec 31  1969 CNAME
		dr-xr-xr-x 2 root root   0 Dec 31  1969 MX
		dr-xr-xr-x 2 root root   0 Dec 31  1969 PTR
		-r--r--r-- 1 root root 658 Dec 31  1969 README
		dr-xr-xr-x 2 root root   0 Dec 31  1969 TXT
		$ ls -l mnt/A
		total 0
		$ cat mnt/A/python.org
		151.101.0.223
		151.101.128.223
		151.101.64.223
		151.101.192.223
		$
2. [Markdown compiling filesystem](./useful/markdown_compiling_fs.py): A
   read-only passthrough filesystem based on
   [5._read_only_passthrough.py](./5._read_only_passthrough.py) that presents
   all Markdown files as if compiled to HTML.

		$ ls README.*
		README.md
		$ ./useful/markdown_compiling_fs.py mnt -o src_dir=.
		$ ls mnt/README.*
		mnt/README.html  mnt/README.md
		$ head -1 mnt/README.md
        # A hand-holding guide to writing FUSE-based filesystems in Python
		$ head -1 mnt/README.html
		<h1>A hand-holding guide to writing FUSE-based filesystems in Python</h1>
		$
3. [Unzip filesystem](./useful/unzipfs.py): Do you want to peek inside a
   compressed (`.zip`) file? using this simple FUSE filesystem you can mount it
   and go! This program is based on
   [4._basic_stat_info.py](4._basic_stat_info.py). 

		$ ./useful/unzipfs.py mnt -o zip_file=compressed_archive.zip
		$ cat mnt/README.md
		(...)

4. [Uncomment filesystem](./useful/uncommentfs.py): Are there too many comments
   in the code, and you want to look at a “clean” version of its contents,
   focusing on what really happens? You can use this filesystem, based on
   [5._read_only_passthrough.py](./5._read_only_passthrough.py). It will present
   the passthrough to the directory from which it is invoked.

		$ ./useful/uncommentfs.py mnt/
		$ grep -v ^$ 4._basic_stat_info.py | head 
		#!/usr/bin/python3
		#
        # Heavily based on Andrew Straw's (<strawman@astraw.com>) code from 2006.
		#
        # Distributed under the terms of the GNU LGPL 2.1 or, at your option,
        # any newer
		import os, stat, errno, fuse, sys
		from datetime import datetime
		from fuse import Fuse
        # We need to declare the FUSE API Python compliance version. (0, 2) is
		$ grep -v ^$ mnt/4._basic_stat_info.py |head
        import os, stat, errno, fuse, sys
        from datetime import datetime
        from fuse import Fuse
        fuse.fuse_python_api = (0, 2)
        class FileData():
            def __init__(self, contents:bytes = b''):
                self.contents = contents
                self.stat = FileStat()
                self.stat.st_size = len(contents)
                self.stat.st_ctime = FileStat.epoch_now()

## My motivation

My main motivation for starting this project is that I teach the
*Operating Systems* subject in [Facultad de Ingeniería, UNAM
(México)](https://www.ingenieria.unam.mx). One of my projects for
evaluating my students is giving them an image and asking them to use it
as a filesystem; I always suggest them to write a FUSE module for it,
but nobody has yet done so. I think a gentle, gradual introduction can
be useful to understand how to achieve this.

## My limitations

I am starting this project in Python, as it is the language of choice
for most of my students. However, I am not that proficient in Python; I
can produce working code, but, as the adage goes, *you can write C in
any language*. My code will most likely not be beautiful by Python
standards, but I will try for it to be clear for newcomers. I am doing,
yes, an effort to comment and document it thoroughly, and to do so
following Python's standards.

## License

I started this project by deriving from Andrew Straw's
(<strawman@astraw.com>) code from 2006,
[hello.py](https://github.com/libfuse/python-fuse/blob/master/example/hello.py). The
licensing for his code (and, thus, for this project's) mandates that
*this program can be distributed under the terms of the GNU LGPL*, and
so is this one.

In the absence of clearer indications from the previous author, I
specify this code can be distributed under the terms of the GNU LGPL 2.1
or, at your option, any newer version. For the full license text, please
refer to the [COPYING](./COPYING) file.
